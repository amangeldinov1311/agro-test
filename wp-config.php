<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе установки.
 * Необязательно использовать веб-интерфейс, можно скопировать файл в "wp-config.php"
 * и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки базы данных
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://ru.wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Параметры базы данных: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'test' );

/** Имя пользователя базы данных */
define( 'DB_USER', 'root' );

/** Пароль к базе данных */
define( 'DB_PASSWORD', '' );

/** Имя сервера базы данных */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу. Можно сгенерировать их с помощью
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}.
 *
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными.
 * Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'J`vvz1&W3Tgfdf+Xn~Nh%tNRR-6SK]EHj{P{o*>N:,lR~JU++c5!v_-in9QB.D m' );
define( 'SECURE_AUTH_KEY',  '_9&01{N5oB_WJ6d4-sx(w},&~96NXdu6`Iq@W6%uf(%Wj?~Q{mR_im#O%UKjG2 x' );
define( 'LOGGED_IN_KEY',    '~+O)&g*kk=Gg :r_Dzb;g=+jdMFf}&P,2HS)R<_@*Z=%9H)oMe.5..*(;uw>A6di' );
define( 'NONCE_KEY',        'lJrzt#y?UQDPF=A0P,(MEx$CQvqV)Mx@.}!K8|WpR3R%QyEq7]Z^8bX<^D>2&.)V' );
define( 'AUTH_SALT',        '=6VV5Y[QnTK(ady36j^<%=<obx|~&-)`LQ{pMAO,5bjsSL&PXdV>{z.St4}hV>5n' );
define( 'SECURE_AUTH_SALT', '`NbS[{6eU F[)a~3d>EJ@5T4(,0kMNB/r>T(:wg*`.NGID`UT%;`$0qpNski1.;p' );
define( 'LOGGED_IN_SALT',   'P-gN$PY`,AH*nD`gyC46w]aRnp%cu+}l ;0CMm.G@MXlqrnD/wmllXsHG3ug4$pA' );
define( 'NONCE_SALT',       'y}c[UnnBA}tK3y=VFh`?Oune2NGZQ+QOk``WHE5d>gPb  I?:-y-Gg7y;+m:suHo' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в документации.
 *
 * @link https://ru.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Произвольные значения добавляйте между этой строкой и надписью "дальше не редактируем". */



/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once ABSPATH . 'wp-settings.php';
